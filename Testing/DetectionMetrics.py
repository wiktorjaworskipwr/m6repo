"""
Testing the Detection Metrics
"""

import cv2
import collections
import numpy as np
import matplotlib.pyplot as plt
from Utilities.funcions import bb_intersect_over_union, f1_score, read_opt_flow

# Seriously, we should add some relative paths
# Importing files

test_1_45 = read_opt_flow('/home/marcinmalak/Desktop/M6 Video Analysis/results_opticalflow_kitti/results/LKflow_000045_10.png')
print(test_1_45)


path_linux = '/home/marcinmalak/Desktop/M6 Video Analysis/AICity_data/AICity_data/train/S03/c010/vdo.avi'
path_windows = 'E:/DATA_Projects/M6/1Week/Data/AICity_data/AICity_data/train/S03/c010/vdo.avi'

cap = cv2.VideoCapture(path_linux)

# Importing Ground truth,
path_linux_gt = '/home/marcinmalak/Desktop/M6 Video Analysis/AICity_data/AICity_data/train/S03/c010/gt/gt.txt'
path_windows_gt = 'E:/DATA_Projects/M6/1Week/Data/AICity_data/AICity_data/train/S03/c010/gt/gt.txt'
gt = {}
with open(path_linux_gt) as f:
    for line in f:
        data = line.split(",")
        # knowing that MOTChallenge format [frame, ID, left, top, width, height, 1, -1, -1, -1]
        # we create dictionary dictionary[frame] = (left,top,width,height)
        gt[int(data[0])] = (int(data[2]), int(data[3]), int(data[4]), int(data[5]))

# Import Detecion
# Importing Ground truth,
path_linux_mask = '/home/marcinmalak/Desktop/M6 Video Analysis/AICity_data/AICity_data/train/S03/c010/det/det_mask_rcnn.txt'
path_windows_mask = "E:/DATA_Projects/M6/1Week/Data/AICity_data/AICity_data/train/S03/c010/det/det_mask_rcnn.txt"
det = collections.defaultdict(list)
with open(path_linux_mask) as f:
    for line in f:
        data = line.split(",")
        # MOTChallenge format [frame, -1, left, top, width, height, conf, -1, -1, -1]
        row = (int(float(data[2])), int(float(data[3])), int(float(data[4])), int(float(data[5])))
        det[int(data[0])].append(row)

# Setting font
font = cv2.FONT_HERSHEY_SIMPLEX

# Skipping to frame 218, because ground truth starts there:
cap.set(cv2.CAP_PROP_POS_FRAMES, 218)

# Init arrays for plots
frameVals = []
F1Vals = []
iouVals = []
truePositives = 0
falsePositives = 0
falseNegative = 0
BOX_DISAPPEAR_CHANCE = 0.95

# Thresholds from 0.5 to 0.95
thresholds = np.linspace(0.5, 1, 10, endpoint=False)
thresholdVals = collections.defaultdict(list)
for thresh in thresholds:
    thresholdVals[thresh] = [0, 0, 0]
    # thresholdVals[0.5] = [TP_count, FP_count, FN_count]

while cap.isOpened():
    ret, frame = cap.read()
    cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('image', 1280, 720)
    currentFrame = cap.get(cv2.CAP_PROP_POS_FRAMES)
    frameText = "Frame:" + str(currentFrame)
    cv2.putText(frame, frameText, (0, 50), font, 1.5, (0, 255, 0), 3, cv2.LINE_AA)
    frameVals.append(currentFrame)

    gt_box = gt.get(currentFrame, None)
    if gt_box:
        # Generating some noise
        mu, sigma = 0, 5
        noise = np.random.normal(mu, sigma, [1, 4])
        xNoise = int(gt_box[0] + noise[0][0])
        yNoise = int(gt_box[1] + noise[0][1])
        widthNoise = int(gt_box[2] + 2.5 * noise[0][2])
        heightNoise = int(gt_box[3] + 2.5 * noise[0][3])

        # Saving point coordinates
        iouGT = (gt_box[0], gt_box[1], gt_box[0] + gt_box[2], gt_box[1] + gt_box[3])
        iouNoise = (xNoise, yNoise, xNoise + widthNoise, yNoise + heightNoise)

        # Calculating IoU
        iouVal = bb_intersect_over_union(iouNoise, iouGT)

        '''
        Wszystko mozna rozjebac potem na funckje i klasy jesli 
        uznajesz ze to jest 'dobra praktyka' w takich sytuacjach.
        '''

        # Random chance of box disappearing, this will give some false negatives
        randomDis = np.random.random_sample()

        # Calculating mAP
        for thresh in thresholds:
            if randomDis > BOX_DISAPPEAR_CHANCE:
                thresholdVals[thresh][2] += 1  # FN, object not found
                pass
            elif iouVal > thresh:
                thresholdVals[thresh][0] += 1  # TP, object detected
            elif iouVal < thresh:
                thresholdVals[thresh][1] += 1  # FP, false detection

        treshold_f1 = 0.90

        try:
            precision = thresholdVals[treshold_f1][0] / (thresholdVals[treshold_f1][0] + thresholdVals[treshold_f1][1])
            recall = thresholdVals[treshold_f1][0] / (thresholdVals[treshold_f1][0] + thresholdVals[treshold_f1][2])
        except ValueError:
            print('put value')

        f1Val = f1_score(precision, recall)
        F1Vals.append(f1Val)

        # Drawing
        iouText = "IoU:" + str(iouVal)
        iouVals.append(iouVal)
        cv2.putText(frame, iouText, (gt_box[0], gt_box[1] - 30), font, 1.0, (0, 255, 0), 3, cv2.LINE_AA)
        cv2.rectangle(frame, (iouGT[0], iouGT[1]), (iouGT[2], iouGT[3]), (0, 255, 0), 3)
        cv2.rectangle(frame, (iouNoise[0], iouNoise[1]), (iouNoise[2], iouNoise[3]), (255, 0, 255), 3)
    else:
        treshold_f1 = 0.90
        thresholdVals[treshold_f1][2] += 1
        try:
            precision = thresholdVals[treshold_f1][0] / (thresholdVals[treshold_f1][0] + thresholdVals[treshold_f1][1])
            recall = thresholdVals[treshold_f1][0] / (thresholdVals[treshold_f1][0] + thresholdVals[treshold_f1][2])
        except ValueError:
            pass
        iouVals.append(0)
        f1Val = f1_score(precision, recall)
        F1Vals.append(f1Val)

    # Drawing detections
    # det_box = det.get(currentFrame, None)
    # if det_box:
    #     for box in det_box:
    #         cv2.rectangle(frame, (box[0], box[1]), (box[0] + box[2], box[1] + box[3]), (255, 0, 255), 3)

    cv2.imshow('image', frame)

    # Saving frames to files
    # cv2.imwrite("frame%d.jpg" % frame_count, frame)

    if cv2.waitKey(10) & 0xFF == ord('q'):
        break

# Plot for IoU over frame
fig = plt.figure("IoU Plot")
plt.title('IoU(frame)')
plt.xlabel('Video frame')
plt.ylabel('IoU Value')
plt.plot(frameVals, iouVals)
plt.show()

# Plot for F1 score per frame
fig = plt.figure("F1 Plot")
plt.title('F1(frame)')
plt.xlabel('Video frame')
plt.ylabel('F1 Measure')
plt.plot(frameVals, F1Vals)
plt.show()

# Plot for mAP
fig = plt.figure("mAP Plot")
for counter, thresh in enumerate(thresholds):
    plt.subplot(thresholds.size / 2, 2, counter + 1)
    labels = ('TP', 'FP', 'FN')
    y_pos = np.arange(len(labels))
    values = [thresholdVals[thresh][0],
              thresholdVals[thresh][1],
              thresholdVals[thresh][2]]
    plt.barh(y_pos, values, align='center')
    plt.yticks(y_pos, labels)
    try:
        precision = values[0] / (values[0] + values[1])
        recall = values[0] / (values[0] + values[2])
    except ValueError:
        title = "mAP for threshold:" + str(thresh)
    precision = round(precision, 3)
    recall = round(recall, 3)
    title = "mAP for threshold:" + str(thresh) + " precision:" + str(precision) + " recall:" + str(recall)

    plt.title(title)
    plt.show()

cap.release()
cv2.destroyAllWindows()





#msen_45 = msen(test_1_45, gt_1)
#pepn_45 = pepn(test_1, gt_1)

#msen_157 = msen(test_2, gt_2)
#pepn_157 = pepn(test_2, gt_2)
