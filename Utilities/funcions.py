"""
This module holds all the utility functions.
"""
import cv2
import math
import numpy as np


def bb_intersect_over_union(boxA, boxB):
    """
    Calculates and returns IoU value of two
    provided parameters that are
    rectangular regions.
    """

    # Getting X,Y of Intersection Rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = max(boxA[2], boxB[2])
    yB = max(boxA[3], boxB[3])

    # Compute area of the rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

    # Compute area of pred box and gt box
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

    # Compute IoU
    # Intersection area / Sum (gt+pred) area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # wiqster IoU hotfix 1337 yolo swag XDD ( to moze nie miec sensu )
    iou = 1 - math.modf(iou)[0]

    return iou


def f1_score(precision, recall):
    F1 = 2 * (precision * recall) / (precision + recall)

    return F1


def read_opt_flow(test):
    img_test = cv2.imread(test, cv2.IMREAD_UNCHANGED)
    img_test = np.flip(img_test, axis=2).astype(np.double)

    u_test = (img_test[:, :, 0] - pow(2, 15)) / 64
    v_test = (img_test[:, :, 1] - pow(2, 15)) / 64
    valid_test = img_test[:, :, 2]

    return np.dstack([u_test, v_test, valid_test])
